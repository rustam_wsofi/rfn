export let initialState ={
    login:{
        user_name:'',
        password:''
    },
    dashboard_status: 'logout',
    login_status:''
  };
export default function(state= initialState, action){
  switch(action.type){
        case 'SET_LOGIN_FIELD_VALUE':
            return {...state, login: {...state.login,[action.payload.field]: action.payload.value}};
        case 'SET_DASHBOARD_REDIRECTION_VALUE':
            sessionStorage.setItem("dashboard_status", action.payload);
            return Object.assign({}, state, {
                dashboard_status: action.payload,
                login_status: action.payload ? '' : 'Incorrect username or password!'
              })
        case 'UNSET_LOGIN_USER_VALUE':
          sessionStorage.setItem("dashboard_status", 'logout');
          return Object.assign({}, state, {
              dashboard_status: false
            })
        default:
        return state;
  }
}