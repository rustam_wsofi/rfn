export let initialState = {
data:null
};
export default function(state= initialState, action){
    switch(action.type){
        case 'STORE_PLANETS_DATA_IN_STORE':
            return Object.assign({}, state, {
                data: action.payload.data,
            })
        default:
            return state
    }
}