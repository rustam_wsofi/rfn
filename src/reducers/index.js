import { combineReducers } from 'redux';
import loginReducer from './login_reducers';
import getPlanets from './planets_reducers'
const rootReducer = combineReducers({
    login: loginReducer,
    planets: getPlanets
});

export default rootReducer;
