import 'babel-polyfill';
import axios from 'axios';
import _ from 'lodash';
const ROOT_URL = 'https://swapi.co/api/';
export async function login_user(requestBody){
    const url = `${ROOT_URL}people/`;
    const response = await axios.get(url);
    var flag = 'logout';
    response.data.results.map(function (element) {
        if(requestBody.user.username === element.name && requestBody.user.password === element.birth_year){
            flag = 'login';
            return true;
        }
    }); 
    return{
        type:'SET_DASHBOARD_REDIRECTION_VALUE',
        payload :flag
    };
}
export function set_login_form_field_vale(data){
    return {
            type:'SET_LOGIN_FIELD_VALUE',
            payload: data
    }
}
export async function get_planets_list(){
    const url = `${ROOT_URL}planets/`;
    const response = await axios.get(url);
    return{
        type:'STORE_PLANETS_DATA_IN_STORE',
        payload :response
    };
}
export async function logout_user(){
    return{
        type:'UNSET_LOGIN_USER_VALUE',
        payload :'logout'
    };
}
