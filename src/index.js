import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import promise from 'redux-promise';
import reducers from './reducers';
import Login from './components/login';
import Planets from './components/planets';
import LandingPage from './components/container/landingpage';
import Registration from './components/container/registration';
const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
   <BrowserRouter>
     <div className="container">
      <Switch>
        <Route path="/planets" component={Planets} />
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Registration} />
        <Route path="/" component={LandingPage} />
      </Switch>
     </div>
   </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
