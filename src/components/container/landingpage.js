import React,{Component} from 'react';
import {Button,Nav, NavItem, Navbar } from 'react-bootstrap';
import Header from './header';
import Footer from './footer'
import { Link } from 'react-router-dom'
class LandingPage extends Component
{
    constructor(props, context) {
        super(props);
    }
    render(){
        console.log(sessionStorage.getItem("dashboard_status"));
        return (
            <div>
            <Header />
            <Nav bsStyle="pills" activeKey={1} >
                <NavItem eventKey={1} href="/" title="Home">
                    Home
                </NavItem>
                {sessionStorage.getItem("dashboard_status") == null || sessionStorage.getItem("dashboard_status") == 'logout' ?
                <NavItem eventKey={2} href="/login" title="Sign In">
                    Sign In
                </NavItem>
                : null }
                <NavItem eventKey={3} href="/registration" title="Registration">
                    Registration
                </NavItem>
                {sessionStorage.getItem("dashboard_status") == 'login' ?
                <NavItem eventKey={4} href="/planets" title="Administraton">
                    Administraton
                </NavItem>
                : null }
            </Nav>
            <Footer />
            </div>
        )
    }
}

export default LandingPage;