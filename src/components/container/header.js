import React, {Component} from 'react';

class Header extends Component {
    render() {
        const sectionStyle = {
            width: "100%",
            height: "50px",
            marginTop:"51px",
            textAlign:"center",
            backgroundColor:"grey",
        };
        return (
            <div className="Header">
                <div className="frontbackground" style={ sectionStyle }>
                   Welcome To Delhi
                </div>
            </div>
        );
    }
}
export default Header;