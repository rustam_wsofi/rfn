import React,{Component} from 'react';
import {Form,PageHeader,FormGroup, ControlLabel,  FormControl, Button,HelpBlock } from 'react-bootstrap';
import InputMask from 'react-input-mask';
class Registration extends Component
{
    constructor(props, context) {
        super(props);
        this.state = ({value : '',first_name : '', first_name_validation:'', last_name:'',last_name_validation:'',email:'', email_validation:'',value_validation:''})
    }
    /**
     * @param  {} event
     * @param  {event.target.value}} =>{this.setState({value
     */
    onChange = (event) => {
        this.setState({value: event.target.value});
    }
    /**
     * @param  {} newState
     * @param  {} oldState
     * @param  {} userInput
     * @param  {null;if(value.endsWith('-'} =>{var{value}=newState;varselection=newState.selection;varcursorPosition=selection?selection.start
     */
    beforeMaskedValueChange = (newState, oldState, userInput) => {
    var { value } = newState;
    var selection = newState.selection;
    var cursorPosition = selection ? selection.start : null;
        // keep minus if entered by user
        if (value.endsWith('-') && userInput !== '-' && !this.state.value.endsWith('-')) {
          if (cursorPosition === value.length) {
            cursorPosition--;
            selection = { start: cursorPosition, end: cursorPosition };
          }
          value = value.slice(0, -1);
        }
        return {
          value,
          selection
        };
      }
    /**
     * @param  {} event
     * @param  {} {if(this.state.first_name_validation!=''&&this.state.last_name_validation!=''
     * @param  {} this.state.email_validation!=''
     * @param  {} this.state.phone_validation!=''
     * @param  {} {console.log('Success'
     * @param  {} ;}else{console.log("Failure"
     */
    __formSubmit(event){
        if(this.state.first_name_validation =='' && this.state.last_name_validation =='', this.state.email_validation =='', this.state.phone_validation =='' && this.state.first_name != '' && this.state.last_name!='' && this.state.email!='' && this.state.phone !=''){
            let email_patern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(email_patern.test(this.state.email)){
                console.log('Success');
            }
            else{
                this.setState({email_validation:"Email format is not correct!"});
                console.log("Failure");
            }
        }else{
            console.log("Failure");
        }
    }
    /**
     * @param  {} event
     * @param  {} {leterror_field=[event.target.name]+'_validation';if(event.target.value==''
     * @param  {"RequiredField!"}} {this.setState({[error_field]
     */
    _validateField(event){
        let error_field = [event.target.name]+'_validation';
        if(event.target.value == ''){
            this.setState({[error_field]:"Required Field!"});
        }else{
            this.setState({[event.target.name]:event.target.value});
            }
    }
    render(){
        const style = {
            formStyle:{maxWidth:'500px',
            margin:'0 auto'},
            errorBlock:{
                color: 'red'
            },
            buttonStyle : {
                marginLeft : '10px'
            }
        }
        return (
            <div>
            <form style = {style.formStyle} >
            <PageHeader> Registration Form </PageHeader>
            <div style = {style.errorBlock}></div>
                <FormGroup 
                    controlId="formValidationSuccess1" 
                    validationState="success">
                    <ControlLabel>First Name</ControlLabel>
                    <FormControl 
                        type="text" 
                        name="first_name" 
                        value = {this.state.first_name}
                        onChange={(e)=>{this._validateField(e)}}
                    />
                    <FormControl.Feedback />
                    <HelpBlock>{this.state.first_name_validation}</HelpBlock>
                </FormGroup>
                <FormGroup 
                    controlId="formValidationSuccess1" 
                    validationState="success">
                    <ControlLabel>Last Name</ControlLabel>
                    <FormControl 
                        type="text" 
                        name="last_name"
                        value={this.state.last_name}
                        onChange={(e)=>{this._validateField(e)}}
                    />
                    <FormControl.Feedback />
                    <HelpBlock>{this.state.last_name_validation}</HelpBlock>
                </FormGroup>
                <FormGroup 
                    controlId="formValidationSuccess1" 
                    validationState="success">
                    <ControlLabel>Email Address</ControlLabel>
                    <FormControl 
                        type="email" 
                        name="email"
                        value={this.state.email}
                        onChange={(e)=>{this._validateField(e)}}
                    />
                    <FormControl.Feedback />
                    <HelpBlock>{this.state.email_validation}</HelpBlock>
                </FormGroup>
                <FormGroup>
                <ControlLabel>Telephone</ControlLabel>
                <InputMask 
                    mask="(999)-999-9999" 
                    maskChar={null} 
                    value={this.state.value} 
                    onChange={this.onChange} 
                    beforeMaskedValueChange={this.beforeMaskedValueChange} />
                <FormControl.Feedback />
                <HelpBlock>{this.state.phone_validation}</HelpBlock>
                </FormGroup>
                <Button 
                    value="Registration"
                    id="registration"
                    bsStyle="primary"
                    className='no-rad'
                    onClick={(e) => {this.__formSubmit(e) }} >
                    Registration
                </Button>
                <a href="/" style={style.buttonStyle}>
                <Button 
                    value="Home"
                    id="home"
                    bsStyle="primary">
                    Goto Home
                </Button>
                </a>
            </form>
            </div>
        )
    }
}

export default Registration;