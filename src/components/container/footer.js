import React, {Component} from 'react';

class Footer extends Component {
    render() {
        const sectionStyle = {
            width: "100%",
            height: "40px",
            marginTop:"51px",
            textAlign:"center",
            backgroundColor:"grey",
        };
        return (
            <div className="Footer">
                <div className="frontbackground" style={ sectionStyle }>
                    <p style={ { color:"white", } }>
                        @Developed By Rustam
                    </p>
                </div>
            </div>
        );
    }
}
export default Footer;