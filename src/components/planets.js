import React,{Component} from 'react';
import {PageHeader, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {get_planets_list, logout_user} from '../actions';
import {BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Redirect } from 'react-router-dom'
export class Planets extends Component {
    constructor(props) {
        super(props)
    }
    /**
     * @param  {} {this.props.get_planets_list(
     */
    componentWillMount(){
        this.props.get_planets_list();
    }
    /**
     * @param  {} event
     * @param  {} {this.props.logout_user(
     */
    __logout(event){
        this.props.logout_user();
    }

    render() {
        const style = {
            buttonStyle : {
                marginLeft : '1000px',
                marginTop: '-90px'
            }
        }
        const  { planets, loginForm } = this.props;
        let data = planets.data != null ? planets.data.results : [];
        if (sessionStorage.getItem("dashboard_status") == 'logout') {
            return <Redirect to='/' />
          }
        return (<div>
            <PageHeader> Population List </PageHeader>
            <Button 
                style={style.buttonStyle}
                value="Logout"
                id="logout"
                bsStyle="primary"
                className='no-rad'
                onClick={(e) => {this.__logout(e) }} >
                Logout
            </Button>
            <BootstrapTable data={ data } pagination>
            <TableHeaderColumn dataField='name' filter={ { type: 'TextFilter', delay: 1000 } }>Name</TableHeaderColumn>
            <TableHeaderColumn dataField='climate' filter={ { type: 'TextFilter', delay: 1000 } }>Climate</TableHeaderColumn>
            <TableHeaderColumn dataField='diameter' isKey filter={ { type: 'TextFilter', delay: 1000 } }>Diameter</TableHeaderColumn>
            <TableHeaderColumn dataField='population' filter={ { type: 'TextFilter', delay: 1000 } }>Population</TableHeaderColumn>
            <TableHeaderColumn dataField='rotation_period' filter={ { type: 'TextFilter', delay: 1000 } }>Rotation Period</TableHeaderColumn>
            <TableHeaderColumn dataField='orbital_period' filter={ { type: 'TextFilter', delay: 1000 } }>Orbital Period</TableHeaderColumn>
          </BootstrapTable>
            </div>)
    }

}
function mapStateToProps(state) {
    return {
        planets: state.planets,
        loginForm: state.login
    }
  }
function mapDispatchToProps(dispatch){
    return bindActionCreators({get_planets_list:get_planets_list, logout_user:logout_user},dispatch);
}
// the default export is the redux-connected component
export default connect(mapStateToProps,mapDispatchToProps)(Planets)