import React,{Component} from 'react';
import {Form,PageHeader,FormGroup, ControlLabel,  FormControl, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login_user, set_login_form_field_vale} from '../actions';
import { Redirect } from 'react-router-dom'
class Login extends Component
{
    constructor(props, context) {
        super(props);
    }
    /**
     * @param  {} field
     * @param  {} event
     * @param  {field} {const{dispatch}=this.propsletdata={field
     * @param  {event.target.value}this.props.set_login_form_field_vale(data} value
     */
    __setFieldValue(field, event) {
        let data = {
            field: field,
            value: event.target.value}
        this.props.set_login_form_field_vale(data);
    }
    /**
     * @param  {} event
     * @param  {{user:loginForm.login.user_name} {const{loginForm}=this.propsletloginData={user
     * @param  {loginForm.login.password}}this.props.login_user(loginData} password
     */
    __formSubmit(event){
        const { loginForm } = this.props
        let loginData = {
                user: {
                    username: loginForm.login.user_name,
                    password: loginForm.login.password
                }}
        this.props.login_user(loginData);
    }
    render(){
        const { loginForm } = this.props
        const style = {
            formStyle:{maxWidth:'500px',
            margin:'0 auto'},
            errorBlock:{
                color: 'red'
            },
            buttonStyle : {
                marginLeft : '10px'
            }
        }
        if (sessionStorage.getItem("dashboard_status") == 'login') {
            return <Redirect to='/planets' />
          }
        return (
            <div>
            <form style = {style.formStyle} >
            <PageHeader> Login </PageHeader>
            <div style = {style.errorBlock}>{loginForm.login_status}</div>
                <FormGroup 
                    controlId="formValidationSuccess1" 
                    validationState="success">
                    <ControlLabel>User Name</ControlLabel>
                    <FormControl 
                        type="text" 
                        name="user_name" 
                        onChange={(e) => { this.__setFieldValue('user_name', e) }}
                        value = {loginForm.login.user_name}
                    />
                </FormGroup>
                <FormGroup 
                    controlId="formValidationSuccess1" 
                    validationState="success">
                    <ControlLabel>Password</ControlLabel>
                    <FormControl 
                        type="password" 
                        name="password" 
                        onChange={(e)=> {this.__setFieldValue('password', e)}}
                        value={loginForm.login.password}
                    />
                </FormGroup>
                <Button 
                    value="Login"
                    id="login"
                    bsStyle="primary"
                    className='no-rad'
                    onClick={(e) => {this.__formSubmit(e) }} >
                    Login
                </Button>
                <a href="/" style={style.buttonStyle}>
                <Button 
                    value="Home"
                    id="home"
                    bsStyle="primary">
                    Goto Home
                </Button>
                </a>
            </form>
            </div>
        )
    }
}
/**
 * @param  {} state
 * @param  {state.userForm};}} {return{userForm
 * @returns state
 */

function mapStateToProps(state) {
    return { loginForm: state.login };
}
/**
 * @param  {} dispatch
 * @param  {login_user}} {returnbindActionCreators({login_user
 * @param  {} dispatch
 */
function mapDispatchToProps(dispatch){
    return bindActionCreators({login_user:login_user, set_login_form_field_vale:set_login_form_field_vale},dispatch);
}
export default (connect(mapStateToProps,mapDispatchToProps)(Login));